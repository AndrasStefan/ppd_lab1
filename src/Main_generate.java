import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Main_generate {
    public static void main(String[] args) {
        int n = 1000;
        int m = 1000;
        String destination = "src/input/mat3.txt";
        String rez = "";

        rez += n + " " + m + "\n";
        for (int i = 1; i <= n; ++i) {
            for (int j = 1; j <= m; ++j) {
                int number = (int) (Math.random() * 100);
                rez += number + " ";
            }
            rez += "\n";
        }


        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(destination));
            writer.write(rez);//save the string representation of the board
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

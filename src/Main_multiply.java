import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Main_multiply {
    public static void main(String[] argc) {
        ArrayList<MultiplyThread> threads = new ArrayList<>();
        Scanner reader = new Scanner(System.in);

        Matrix matA = Matrix.read_from_file("src/input/mat9.txt");
        Matrix matB = Matrix.read_from_file("src/input/mat10.txt");

        int na = matA.n;
        int ma = matA.m;

        int nb = matB.n;
        int mb = matB.m;

        Matrix output = new Matrix(na, mb);

        if (ma != nb) {
            System.out.println("Matricile nu se pot inmulti");
        }

        System.out.print("Nr de thread-uri = ");
        int nt = reader.nextInt();

        int nr = Math.round((na * mb) / (float) nt);

        for (int i = 1; i <= nt; ++i) {
            MultiplyThread t;
            if (i == nt) {
                t = new MultiplyThread(i, (i - 1) * (int) nr, na * mb, matA, na, ma, matB, nb, mb, output);
            } else {
                t = new MultiplyThread(i, (i - 1) * (int) nr, i * (int) nr, matA, na, ma, matB, nb, mb, output);
            }

            threads.add(t);
        }

        long startTime = System.nanoTime();

        for (int i = 1; i <= nt; ++i) {
            threads.get(i - 1).start();
        }

        for (int i = 1; i <= nt; ++i) {
            try {
                threads.get(i - 1).join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        long endTime   = System.nanoTime();
        long totalTime = endTime - startTime;
        long ms = TimeUnit.MILLISECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
        System.out.println("Timpul de executie este: " + ms + " milisec");

//        matA.print();
//        matB.print();
//        output.print();
    }
}

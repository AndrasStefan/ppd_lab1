import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Main_sum {
    public static void main(String[] args) {
        ArrayList<SumThread> threads = new ArrayList<>();
        Scanner reader = new Scanner(System.in);

        Matrix matA = Matrix.read_from_file("src/input/mat_100.txt");
        Matrix matB = Matrix.read_from_file("src/input/mat_100.txt");

        int na = matA.n;
        int ma = matA.m;

        int nb = matB.n;
        int mb = matB.m;

        Matrix output = new Matrix(na, mb);

        if(na != nb && ma != mb)
        {
            System.out.println("Matricile nu se pot aduna");
        }


        System.out.print("Nr de thread-uri = ");
        int nt = reader.nextInt();

        int nr = Math.round((na * ma) / (float)nt);

        for(int i = 1; i <= nt; ++i)
        {
            SumThread t;
            if(i == nt)
            {
                t = new SumThread(i, (i - 1) * (int)nr, na * ma, matA, na, ma, matB, nb, mb, output);
            }
            else
            {
                t = new SumThread(i, (i - 1) * (int)nr, i * (int)nr, matA, na, ma, matB, nb, mb, output);
            }

            threads.add(t);
        }

        long startTime = System.nanoTime();

        for(int i = 1; i <= nt; ++i)
        {
            threads.get(i - 1).start();
        }

        for(int i = 1; i <= nt; ++i)
        {
            try
            {
                threads.get(i - 1).join();
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }

        long endTime   = System.nanoTime();
        long totalTime = endTime - startTime;
        long ms = TimeUnit.MILLISECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
        System.out.println("Timpul de executie este: " + ms + " milisec");

//        matA.print();
//        matB.print();
//        output.print();
    }
}

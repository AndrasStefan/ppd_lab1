import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

class Matrix {
    int n, m;
    int[][] values;

    Matrix(int lines, int columns) {
        n = lines;
        m = columns;
        this.values = new int[lines + 1][columns + 1];
    }

    void print() {
        for (int i = 1; i <= n; ++i) {
            for (int j = 1; j <= m; ++j) {
                System.out.format("%6d ", values[i][j]);
            }
            System.out.println();
        }
        System.out.println();
    }


    static Matrix read_from_file(String pathname) {
        Scanner input = null;
        try {
            input = new Scanner(new File(pathname));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        int n = input.nextInt();
        int m = input.nextInt();

        Matrix rez = new Matrix(n, m);

        for (int i = 1; i <= n; ++i) {
            for (int j = 1; j <= m; ++j) {
                if (input.hasNextInt()) {
                    rez.values[i][j] = input.nextInt();
                }
            }
        }

        return rez;
    }
}

public class SumThread extends Thread {
    private int id = 0;

    private int indexStart, indexEnd;

    private Matrix matA, matB;

    private int na, ma, nb, mb;

    private Matrix m;

    SumThread(Integer id, int rowStart, int rowEnd, Matrix matA, int na, int ma, Matrix matB, int nb, int mb, Matrix m)
    {
        this.id = id;
        this.indexStart = rowStart;
        this.indexEnd = rowEnd;
        this.matA = matA;
        this.na = na;
        this.ma = ma;
        this.matB = matB;
        this.nb = nb;
        this.mb = mb;
        this.m = m;
    }

    @Override
    public void run()
    {
        super.run();

        for(int index = indexStart; index < indexEnd; ++index)
        {
            int line = index / mb + 1;
            int column = index % mb + 1;

            m.values[line][column] = matA.values[line][column] + matB.values[line][column];
        }
    }
}
